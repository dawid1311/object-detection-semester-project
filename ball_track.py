import cv2 as cv
import numpy as np
import cv2 as cv
from detection import HAAR_Detection
from tracking import Tracking
from keywords import Shapes as SHAPE
from settings import Settings
from keywords import Keys as KEY
from keywords import Colors as COLOR
import sys
from kalman_filter_old import KalmanFilter

# MIN_H_BLUE = 200
# MAX_H_BLUE = 300
def nothing(value):
    pass


def noise_remove(thresh):
    kernel = np.ones((3, 3), np.uint8)
    opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel, iterations=2)
    # sure background area
    sure_bg = cv.dilate(opening, kernel, iterations=3)
    # Finding sure foreground area
    dist_transform = cv.distanceTransform(opening, cv.DIST_L2, 5)
    ret, sure_fg = cv.threshold(dist_transform, 0.7 * dist_transform.max(), 255, 0)
    # Finding unknown region
    sure_fg = np.uint8(sure_fg)
    return cv.add(sure_bg, sure_fg)


kalman_filter = KalmanFilter('kalman', COLOR.WHITE, SHAPE.RECTANGLE)
settings = Settings()

# open vid file
cap = cv.VideoCapture("resources/lemon.mp4")

vid_end, frame = cap.read()
if not vid_end:
    print('Error during loading video')
    sys.exit(0)
elif vid_end and settings.save:
    # prepare for saving video
    vid_res = (int(cap.get(cv.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)))
    out = cv.VideoWriter(settings.save_path, cv.VideoWriter_fourcc(*"XVID"), cap.get(cv.CAP_PROP_FPS), vid_res)


cv.namedWindow('vid_frame')
cv.namedWindow("bar_win")

cv.createTrackbar('min_hue', 'bar_win', 55, 180, nothing)
cv.createTrackbar("max_hue", "bar_win", 60, 255, nothing)
cv.createTrackbar("min_sat", "bar_win", 60, 255, nothing)
cv.createTrackbar('max_sat', 'bar_win', 255, 255, nothing)
cv.createTrackbar('min_value', 'bar_win', 40, 255, nothing)
cv.createTrackbar("max_value", "bar_win", 255, 255, nothing)
tracked_roi = None
frame_counter = 0
is_object_in_sight = None


while True:
    # read vid frame
    vid_end, frame = cap.read()
    if vid_end:

        frame_to_draw = frame.copy()
        dt = settings.frame_time / 1000
        output = frame.copy()



        min_hue = cv.getTrackbarPos('min_hue', 'bar_win')
        min_sat = cv.getTrackbarPos("min_sat", "bar_win")
        min_value = cv.getTrackbarPos("min_value", "bar_win")
        max_hue = cv.getTrackbarPos("max_hue", "bar_win")
        max_sat = cv.getTrackbarPos("max_sat", "bar_win")
        max_value = cv.getTrackbarPos("max_value", "bar_win")

        # filtering image
        frame_HSV = cv.cvtColor(frame.copy(), cv.COLOR_BGR2HSV)
        frame_threshold = cv.inRange(frame_HSV, (min_hue, min_sat, min_value), (max_hue, max_sat, max_value))
        cv.imshow('vid_thresh', frame_threshold)
        # frame_threshold = noise_remove(frame_threshold)

        contours, hierarchy = cv.findContours(frame_threshold, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        output = cv.drawContours(output, contours, -1, (255, 0, 0), 3)

        boundRect = [None]*len(contours)
        contours_poly = [None] * len(contours)

        for i, c in enumerate(contours):
            contours_poly[i] = cv.approxPolyDP(c, 3, True)
            boundRect[i] = cv.boundingRect(contours_poly[i])
        drawing = np.zeros((output.shape[0], output.shape[1], 3), dtype=np.uint8)

        if kalman_filter.initial_state is None and tracked_roi is not None:
            kalman_filter.initial_state = tracked_roi
        if kalman_filter.initial_state is not None:
            kalman_filter.predict_and_draw(frame_to_draw, dt)
        if len(boundRect) > 0:
            tracked_roi = (int(boundRect[0][0]), int(boundRect[0][1]), int(boundRect[0][0] + boundRect[0][2]),
                       int(boundRect[0][1] + boundRect[0][3]))
            cv.rectangle(frame, (int(boundRect[0][0]), int(boundRect[0][1])),
                         (int(boundRect[0][0] + boundRect[0][2]), int(boundRect[0][1] + boundRect[0][3])), (0, 0, 255),
                         2)
            is_object_in_sight = True
        else:
            is_object_in_sight = False
        if kalman_filter.initial_state is not None:
            kalman_filter.update_state(tracked_roi, is_object_in_sight)


        if settings.save:
            out.write(frame_to_draw)
        cv.imshow('vid_bound', output)
        cv.imshow('frame', frame_to_draw)
        cv.imshow('vid_thresh', frame_threshold)
        key = cv.waitKey(settings.frame_time)
        # press ecc to exit
        if key == 27:
            print("User ended program with ESC button press")
            break
        print(frame_counter)
        frame_counter += 1
    else:
        print("Video ended")
        break
if settings.save:
    out.release()
cap.release()
cv.destroyAllWindows()