import cv2 as cv
import numpy as np
from drawing import Drawing
from keywords import Shapes as SHAPE, Colors as COLOR


class KalmanFilter(Drawing):
    def __init__(self, object_name=None, contour_color=(255, 0, 0), contour_shape=SHAPE.RECTANGLE):
        super().__init__(object_name, contour_color, contour_shape)
        self._kalman_object = self._init_kalman_filter()
        self.actual_state = np.zeros((6, 1), np.float32)
        self.measurement = np.zeros((4, 1), np.float32)
        self.initial_state = None
        self.was_found = False
        self.not_found_count = 0

    # function used to initialise kalman filter object
    def _init_kalman_filter(self):

        kalman = cv.KalmanFilter(6, 4)
        kalman.measurementMatrix = np.array([[1, 0, 0, 0, 0, 0],  # tu wypełniam te macierze potrzebne do inicjalizajcji filtra
                                             [0, 1, 0, 0, 0, 0],
                                             [0, 0, 0, 0, 1, 0],
                                             [0, 0, 0, 0, 0, 1]], np.float32)

        kalman.transitionMatrix = np.array([[1, 0, 0, 0, 0, 0],
                                            [0, 1, 0, 0, 0, 0],
                                            [0, 0, 1, 0, 0, 0],
                                            [0, 0, 0, 1, 0, 0],
                                            [0, 0, 0, 0, 1, 0],
                                            [0, 0, 0, 0, 0, 1]], np.float32)

        kalman.processNoiseCov = np.array([[1, 0, 0, 0, 0, 0],
                                           [0, 1, 0, 0, 0, 0],
                                           [0, 0, 1, 0, 0, 0],
                                           [0, 0, 0, 1, 0, 0],
                                           [0, 0, 0, 0, 1, 0],
                                           [0, 0, 0, 0, 0, 1]], np.float32) * 0.03

        kalman.measurementNoiseCov = cv.setIdentity(kalman.measurementNoiseCov, .1)

        return kalman

    def kalman_filter_transitionMatrix(self, dt): # funkcja wpisująca upływ czasu to tej transitionMatrix z inicjalizajc,
        # self._kalman_object.transitionMatrix[0, 2] = dt  # dla Kalmana (6, 2)
        # self._kalman_object.transitionMatrix[0, 4] = 0.5*dt ** 2
        # self._kalman_object.transitionMatrix[1, 3] = dt
        # self._kalman_object.transitionMatrix[1, 5] = 0.5*dt ** 2
        # self._kalman_object.transitionMatrix[2, 4] = dt
        # self._kalman_object.transitionMatrix[3, 5] = dt

        self._kalman_object.transitionMatrix[0, 2] = dt  # dla Kalmana (4, 2)
        self._kalman_object.transitionMatrix[1, 3] = dt


    def kalman_filter_errorCovPre(self):
        self._kalman_object.errorCovPre[0, 0] = 1  # a tu wprowadzam 1 do tej maceirzy błędu kowariancji, narazie nie używana
        self._kalman_object.errorCovPre[1, 1] = 1
        self._kalman_object.errorCovPre[2, 2] = 1
        self._kalman_object.errorCovPre[3, 3] = 1
        self._kalman_object.errorCovPre[4, 4] = 1
        self._kalman_object.errorCovPre[5, 5] = 1

    def kalman_initStateCalc(self, initState):  # liczenie środka pierwszej detekcji
        xA, yA, xB, yB = initState
        x = ((xB-xA)/2)+xA
        y = ((yB-yA)/2)+yA
        initState_calc = np.array([np.float32(x), np.float32(y)], np.float32)
        return initState_calc

    # function used to calculate prediction of kalman filter
    def predict_and_draw(self, frame_to_draw, dt):
        if self.was_found:
            self.kalman_filter_transitionMatrix(dt)
            self.actual_state = self._kalman_object.predict()
            w = self.actual_state[4]
            h = self.actual_state[5]
            x = int((self.actual_state[0] + self.initial_state[0]) - w / 2)
            y = int((self.actual_state[1] + self.initial_state[1]) - h / 2)
            self.actual_state[0] = self.actual_state[0] + self.initial_state[0]
            self.actual_state[1] = self.actual_state[1] + self.initial_state[1]
            self._draw_contour(frame_to_draw, x, y, w, h)

    def update_state(self, filter_ROI, is_object_in_sight):
        if is_object_in_sight:
            x, y, w, h = filter_ROI
            roi_points = filter_ROI
            center = self._get_rect_center(roi_points)
            center[0] = center[0] - self.initial_state[0]
            center[1] = center[1] - self.initial_state[1]

            self.not_found_count = 0
            self.measurement[0, 0] = (x - self.initial_state[0]) + w / 2
            self.measurement[1, 0] = (y - self.initial_state[1]) + h / 2
            self.measurement[2, 0] = w
            self.measurement[3, 0] = h
            if not self.was_found:
                self.kalman_filter_errorCovPre()
                self.actual_state[0] = self.measurement[0, 0]
                self.actual_state[1] = self.measurement[1, 0]
                self.actual_state[2] = 0
                self.actual_state[3] = 0
                self.actual_state[4] = self.measurement[2, 0]
                self.actual_state[5] = self.measurement[3, 0]
                self._kalman_object.statePost = self.actual_state
                self.was_found = True
            else:
                self._kalman_object.correct(self.measurement)
        else:
            self.not_found_count += 1
            if self.not_found_count >= 100:
                self.was_found = False
                self.not_found_count = 0
