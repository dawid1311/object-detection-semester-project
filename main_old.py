import cv2 as cv
import numpy as np
from detection import HAAR_Detection
from tracking import Tracking
from keywords import Shapes as SHAPE
from settings import Settings
from keywords import Keys as KEY
from keywords import Colors as COLOR
import sys
from kalman_filter_old import KalmanFilter


# mouse callback that lets user to choose which object to track
def chose_ROI(event, x, y, flags, param):
    global detected_objects, is_tracking_active, tracking, frame

    # if tracking is not active and left button was pressed
    if event is cv.EVENT_LBUTTONDOWN and not is_tracking_active:
        for (roi_x, roi_y, roi_width, roi_height) in detected_objects:

            # check which object user selected
            if roi_x <= x <= roi_x + roi_width:
                if roi_y <= y <= roi_y + roi_height:

                    # initialize tracking with selected object
                    tracking.init_tracker(frame, (roi_x, roi_y, roi_width, roi_height))
                    is_tracking_active = True
                    break

def noise_remove(frame):
    kernel = np.ones((3, 3), np.uint8)
    ret, thresh = cv.threshold(frame, 200, 255, cv.THRESH_BINARY)
    erode = cv.erode(thresh, kernel)
    dilate = cv.dilate(erode, kernel)
    opening = cv.morphologyEx(dilate, cv.MORPH_OPEN, kernel, iterations=1)
    closing = cv.morphologyEx(opening, cv.MORPH_CLOSE, kernel, iterations=1)
    return closing
    # sure background area



def main():
    global detected_objects, tracking, detection, frame, cascade, is_tracking_active, test_obstacle

    # initialize necessary objects
    settings = Settings()
    detection = HAAR_Detection(cascade, 'face', COLOR.BLUE, SHAPE.RECTANGLE)
    tracking = Tracking('face', COLOR.GREEN, SHAPE.RECTANGLE, settings.tracking_algorithm)
    kalman_filter = KalmanFilter('kalman', COLOR.WHITE, SHAPE.RECTANGLE)
    tracking_counter = 0
    is_object_in_sight = None

    backSub = cv.createBackgroundSubtractorKNN()

    # open vid file
    cap = cv.VideoCapture(settings.vid_path)

    # check vid, prepare saving and displaying
    vid_end, frame = cap.read()
    if not vid_end:
        print('Error during loading video')
        sys.exit(0)
    elif vid_end and settings.save:
        # prepare for saving video
        vid_res = (int(cap.get(cv.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)))
        out = cv.VideoWriter(settings.save_path, cv.VideoWriter_fourcc(*"XVID"), cap.get(cv.CAP_PROP_FPS), vid_res)

    # define window name
    cv.namedWindow('vid_frame')

    # check if cascade is loading properly
    if not cascade.load(cv.samples.findFile(settings.cascade_path)):
        print('Error loading cascade')
        exit(0)

    # set callback that will tell which object user wants to track
    cv.setMouseCallback('vid_frame', chose_ROI)

    # main program loop
    while True:
        # read vid frame
        vid_end, frame = cap.read()

        dt = settings.frame_time/1000
        # Check for vid end
        if vid_end:
            frame_to_draw = frame.copy()
            frame_gray = cv.cvtColor(frame.copy(), cv.COLOR_BGR2GRAY)
            fgMask = backSub.apply(frame_gray)
            fgMask = noise_remove(fgMask)
            edges = cv.Canny(fgMask, 50, 190, 3)
            contours, hierarchy = cv.findContours(edges, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
            number_of_filtered_conturs = 0
            for contour in contours:
                area = cv.contourArea(contour)
                if area > 200:
                    number_of_filtered_conturs += 1
            if number_of_filtered_conturs > 0:
                is_object_in_sight = True
            else:
                is_object_in_sight = False
                cv.putText(frame_to_draw, "Object out of sight", (20, 60), cv.FONT_HERSHEY_COMPLEX_SMALL, 1,
                           COLOR.RED)






            if not is_tracking_active:
                # if tracking mode is not active detect objects and wait for user input to activate tracking
                detection.detect_and_draw(frame, frame_to_draw)
                detected_objects = detection.get_detected_objects()
            else:

                # get initial state
                if kalman_filter.initial_state is None:
                    kalman_filter.initial_state = tracking.get_trackedROI()

                # preforming prediction
                kalman_filter.predict_and_draw(frame_to_draw, dt)
                # if tracking is active
                tracking.track_and_draw(frame, frame_to_draw)
                kalman_filter.update_state(tracking.get_trackedROI(), is_object_in_sight)

                # if tracking is active for indicated amount of frames perform detection to correct tracking error
                if tracking_counter is settings.detection_rate - 1:
                    detected_ROIs = (detection.detect(frame))
                    # check if there is any object detected, if not pass
                    if len(detected_ROIs) is not 0:

                        # to ensure that detected and tracked objects are the same object, choose detected
                        # ROI that is the nearest to the tracked object
                        nearest_ROI_dict = tracking.find_nearest(detected_ROIs)

                        # if error is larger than maximal value reinitialise tracker (because there is no
                        # other option to reset tracker)
                        if nearest_ROI_dict['distance'] > settings.max_error:
                            tracking.init_tracker(frame, tuple(nearest_ROI_dict['detected_ROI']))

                # count frames without detection
                tracking_counter = (tracking_counter + 1) % settings.detection_rate


            # save output if user wants it
            if settings.save:
                out.write(frame_to_draw)
            cv.imshow('vid_frame', frame_to_draw)
            # cv.imshow('vid_frame_clean', frame)
            # cv.imshow('fgMask', fgMask)
            key = cv.waitKey(settings.frame_time)
            # press ecc to exit
            if key == 27:
                print("User ended program with ESC button press")
                break
            # pres Q to delete current tracker
            elif key is KEY.Q:
                is_tracking_active = False
        else:
            print("Video ended")
            break
    if settings.save:
        out.release()
    cap.release()
    cv.destroyAllWindows()


# global variables
detected_objects = None
is_tracking_active = False
cascade = cv.CascadeClassifier()
detection = None
tracking = None
frame = None


if __name__ == '__main__':
    main()




